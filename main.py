# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

from scripts.core.calculate import Calculate
obj = Calculate()
# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    shape = input("enter shape name")
    if shape == "circle":
        radius1 = int(input("enter radius"))
        print(obj.circle(radius1))
    elif shape == "rectangle":
        length1 = int(input("enter len/gth"))
        breadth1 = int(input("enter breadth"))
        print(obj.rectangle(length1, breadth1))
    elif shape == "square":
        side1 = int(input("enter side of the square"))
        print(obj.square(side1))
    elif shape == "triangle":
        side1 = int(input("enter side_a"))
        side2 = int(input("enter side_b"))
        side3 = int(input("enter side_h"))
        print(obj.triangle(side1, side2, side3))

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
