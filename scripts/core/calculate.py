class Calculate:
    @staticmethod
    def circle(radius):
        perimeter = 2 * 3.14 * radius
        area = 3.14 * radius * radius
        dic = dict()
        dic["area"] = area
        dic["perimeter"] = perimeter
        return dic
        # print('perimeter=', perimeter, " area= ", area)

    @staticmethod
    def rectangle(length, breadth):
        perimeter = 2 * (length + breadth)
        area = length * breadth
        print('perimeter=', perimeter, " area= ", area)
        dic = dict()
        dic["area"] = area
        dic["perimeter"] = perimeter
        return dic

    @staticmethod
    def square(side):
        perimeter = 4 * side
        area = side * side
        # print('perimeter=', perimeter, " area= ", area)
        dic = dict()
        dic["area"] = area
        dic["perimeter"] = perimeter
        return dic

    @staticmethod
    def triangle(side_a, side_b, side_h):
        perimeter = side_a + side_b + side_h
        area = 0.5 * side_b * side_h
        dic = dict()
        dic["area"] = area
        dic["perimeter"] = perimeter
        return dic
        # print('perimeter=', perimeter, " area= ", area)
